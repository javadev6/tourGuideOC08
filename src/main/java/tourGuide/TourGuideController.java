package tourGuide;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.Location;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import gpsUtil.location.VisitedLocation;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.*;
import tripPricer.Provider;

@RestController
public class TourGuideController {

    //executor service

	@Autowired
	private TourGuideService tourGuideService;

    @Autowired
    private RewardsService rewardsService;
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }
    
    @RequestMapping("/getLocation") 
    public String getLocation(@RequestParam String userName) throws ExecutionException, InterruptedException {
        User user = getUser(userName);
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
		return JsonStream.serialize(visitedLocation.location);
    }
    
    //  TODO: Change this method to no longer return a List of Attractions.
 	//  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
 	//  Return a new JSON object that contains:
    	// Name of Tourist attraction, 
        // Tourist attractions lat/long, 
        // The user's location lat/long, 
        // The distance in miles between the user's location and each of the attractions.
        // The reward points for visiting each Attraction.
        //    Note: Attraction reward points can be gathered from RewardsCentral
    @RequestMapping("/getNearbyAttractions") //verifier avec postman
    public String getNearbyAttractions(@RequestParam String userName) {
        AttractionAdvisedDTO attractionAdvisedDTO = tourGuideService.getAttractionAdvised(userName);
        return JsonStream.serialize(attractionAdvisedDTO);
    }

    //verifier sur postman car aucune donnée
    @GetMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        User user = getUser(userName);
    	return JsonStream.serialize(tourGuideService.getUserRewards(user));
        //return JsonStream.serialize(user.getUserRewards());
    }
    
    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
    	// TODO: Get a list of every user's most recent location as JSON
    	//- Note: does not use gpsUtil to query for their current location, 
    	//        but rather gathers the user's current location from their stored location history.
    	//
    	// Return object should be the just a JSON mapping of userId to Locations similar to:
    	//     {
    	//        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371} 
    	//        ...
    	//     }
        Map<String, Location> allcurrentLocations = tourGuideService.getAllCurrentLocations();
    	return JsonStream.serialize(allcurrentLocations);
    }
    
    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
    	List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
    	return JsonStream.serialize(providers);
    }

    //userPreference, verifier sur postman
    @PostMapping("/getUserPreferences")
    public UserPreferences getUserPreference(@RequestBody UserPreferenceDTO userPreferenceDTO){
        UserPreferences userPreference = tourGuideService.updatePreferencesUser(userPreferenceDTO);
        return userPreference;
    }


    private User getUser(String userName) {
    	return tourGuideService.getUser(userName);
    }

   @GetMapping("/getJsonUserFormat")
    public String getUserJsonFormat(){
        UserPreferenceDTO userPreferenceDTO = new UserPreferenceDTO(); //changer le test qui est @Ignor
        userPreferenceDTO.setUserName("Mohammed");
        userPreferenceDTO.setLowerPricePoint(50);
        userPreferenceDTO.setHighPricePoint(200);
        userPreferenceDTO.setTripDuration(2);
        userPreferenceDTO.setTicketQuantity(2);
        userPreferenceDTO.setNumberOfChildren(0);
        userPreferenceDTO.setNumberOfAdults(2);
        return JsonStream.serialize(userPreferenceDTO);
   }

   @GetMapping("/getUser")
    public User getUserFormat(@RequestParam String userName){
        User user = getUser(userName);
       System.out.println(user.getUserRewards());
        return user;
   }

}