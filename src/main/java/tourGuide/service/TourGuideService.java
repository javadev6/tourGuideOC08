package tourGuide.service;

import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.user.*;
import tripPricer.Provider;
import tripPricer.TripPricer;

import javax.money.Monetary;

@Service
public class TourGuideService extends Thread{

	ExecutorService executorService = Executors.newFixedThreadPool(100) ;


	// Function to limit Attraction to 5
	public static Stream<Attraction> limiting_func(Stream<Attraction> ss, int range){
		return ss.limit(range);
	}
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;
	
	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;
		
		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}
	
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	public VisitedLocation getUserLocation(User user) throws ExecutionException, InterruptedException {
		VisitedLocation visitedLocation = null;
		visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() :
				trackUserLocation(user).get();//get()
		return visitedLocation;
	}
	
	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}
	
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}
	
	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}
	
	public List<Provider> getTripDeals(User user) {
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(), 
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}
	
	public  CompletableFuture<VisitedLocation> trackUserLocation(User user) throws ExecutionException, InterruptedException{
		CompletableFuture<VisitedLocation> future = CompletableFuture.supplyAsync(
												()-> {
													VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
													user.addToVisitedLocations(visitedLocation);
													rewardsService.calculateRewards(user);
													return visitedLocation;
												},
				executorService);
		return future;
	}
	
	public void awaitTerminationAfterShutdown() {
		executorService.shutdown();
		try {
			if (!executorService.awaitTermination(5, TimeUnit.MINUTES)) {
				executorService.shutdownNow();
			}
		} catch (InterruptedException ex) {
			executorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}

	/*******************Preference of User************************************************/

	public UserPreferences updatePreferencesUser(UserPreferenceDTO userPreferenceDTO){
		String userName = userPreferenceDTO.getUserName();
		User user = getUser(userName);
		UserPreferences userPreferences = user.getUserPreferences();

		if(userPreferenceDTO.getHighPricePoint() > 0){
			userPreferences.setHighPricePoint(Money.of(userPreferenceDTO.getHighPricePoint(),"USD"));
		}if(userPreferenceDTO.getLowerPricePoint() > 0){
			userPreferences.setLowerPricePoint(Money.of(userPreferenceDTO.getLowerPricePoint(),"USD"));
		}if (userPreferenceDTO.getNumberOfAdults() >= 0){
			userPreferences.setNumberOfAdults(userPreferenceDTO.getNumberOfAdults());
		}if (userPreferenceDTO.getNumberOfChildren() >= 0){
			userPreferences.setNumberOfChildren(userPreferenceDTO.getNumberOfChildren());
		}if (userPreferenceDTO.getTripDuration() >= 0){
			userPreferences.setTripDuration(userPreferenceDTO.getTripDuration());
		}if (userPreferenceDTO.getTicketQuantity() >= 0){
			userPreferences.setTicketQuantity(userPreferenceDTO.getTicketQuantity());
		}
		return userPreferences;
	}

	/*******************Calculate 5 attraction for User***********************************/

	public AttractionAdvisedDTO getAttractionAdvised(String userName){
		AttractionAdvisedDTO attractionAdvisedDTO = new AttractionAdvisedDTO();
		//localiser l'utilisateur
		User user = getUser(userName);
		//recuperer toutes les atractions à proximités
		VisitedLocation visitedLocation = getUser(userName).getLastVisitedLocation(); //position de la derniere visite
		Location location = visitedLocation.location;// position recuperé
		List<AttractionAdvised> attractions = new CopyOnWriteArrayList<>();
		List<Attraction> nearByAttractions = getNearByAttractions(visitedLocation);
		for(Attraction attraction : nearByAttractions){

			AttractionAdvised attractionAdvised = new AttractionAdvised();
			attractionAdvised.setName(attraction.attractionName);
			attractionAdvised.setDistance(rewardsService.getDistance(attraction,location));
			attractionAdvised.setLocation(attraction.latitude,attraction.longitude);
			attractionAdvised.setRewardPoints(rewardsService.getRewardPoints(attraction,user));
			attractions.add(attractionAdvised);
		}
		attractionAdvisedDTO.setAttractionList(attractions);
		attractionAdvisedDTO.setUserLocation(location);
		return attractionAdvisedDTO;
	}

	public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
		List<Attraction> allAtration = gpsUtil.getAttractions();
		return allAtration.stream()
				.sorted(Comparator.comparing(attraction -> rewardsService.getDistance(visitedLocation.location, attraction)))
				.limit(5)
				.collect(Collectors.toList());
	}

	/***********************AllCurrentLocation****************************************/
	public Map<String,Location> getAllCurrentLocations(){
		Map<String, Location> allLocations = new ConcurrentHashMap<>();
		getAllUsers().forEach(user -> {
			allLocations.put(user.getUserId().toString(),user.getLastVisitedLocation().location);
		});
		return allLocations;
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();
	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);
			
			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}
	
	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i-> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}
	
	private double generateRandomLongitude() {
		double leftLimit = -180;
	    double rightLimit = 180;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
	    double rightLimit = 85.05112878;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
	    return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}
	
}
