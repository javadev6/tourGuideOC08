package tourGuide;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.javamoney.moneta.Money;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserPreferenceDTO;
import tourGuide.user.UserPreferences;
import tripPricer.Provider;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

public class TestTourGuideService {

	public TourGuideService tourGuideService;

	@Before
	public  void setUp(){
		Locale.setDefault(Locale.US);
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		tourGuideService = new TourGuideService(gpsUtil, rewardsService);
	}

	@Test
	public void getUserLocation() throws ExecutionException, InterruptedException {
		//UUID.randomUUID()
		UUID uuid = UUID.randomUUID();
		User user = new User(uuid, "jon", "000", "jon@tourGuide.com");
		tourGuideService.tracker.stopTracking();
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();//get()

		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}
	
	@Test
	public void addUser() {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		User retrivedUser = tourGuideService.getUser(user.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

		tourGuideService.tracker.stopTracking();
		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}
	
	@Test
	public void getAllUsers() {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		List<User> allUsers = tourGuideService.getAllUsers();

		tourGuideService.tracker.stopTracking();
		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}
	
	@Test
	public void trackUser() throws ExecutionException, InterruptedException {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();//get()
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}

	@Test
	public void getNearbyAttractions() throws ExecutionException, InterruptedException {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();//get()
		List<Attraction> attractions = tourGuideService.getNearByAttractions(visitedLocation);
		tourGuideService.tracker.stopTracking();
		assertEquals(5, attractions.size());
	}

	@Test
	public void userPreferenceUpdateTest(){
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		tourGuideService.addUser(user);
		CurrencyUnit usd = Monetary.getCurrency("USD");
		//UserPreferences
		UserPreferences userPreferences = new UserPreferences();
		userPreferences.setLowerPricePoint(Money.of(50, usd));
		userPreferences.setHighPricePoint(Money.of(200, usd));
		userPreferences.setTicketQuantity(1);
		userPreferences.setNumberOfChildren(0);
		userPreferences.setNumberOfAdults(1);
		userPreferences.setAttractionProximity(7);
		user.setUserPreferences(userPreferences);
		String beforUpdate = String.valueOf(user.getUserPreferences().getHighPricePoint().getNumber());

		//UserPreferenceDTO
		UserPreferenceDTO userPreferenceDTO = new UserPreferenceDTO();
		userPreferenceDTO.setLowerPricePoint(20);
		userPreferenceDTO.setHighPricePoint(500);
		userPreferenceDTO.setUserName("jon");
		userPreferenceDTO.setNumberOfAdults(2);
		userPreferenceDTO.setNumberOfChildren(1);
		userPreferenceDTO.setTripDuration(2);

		UserPreferences userPreferencesUpdate = tourGuideService.updatePreferencesUser(userPreferenceDTO);
		String afterUpdate = String.valueOf(user.getUserPreferences().getHighPricePoint().getNumber());
		tourGuideService.awaitTerminationAfterShutdown();

		assertEquals(2, userPreferencesUpdate.getNumberOfAdults());
		//assertEquals(userPreferenceDTO.getHighPricePoint(), userPreferencesUpdate.getHighPricePoint());
		assertEquals("200", beforUpdate);
		assertEquals("500",afterUpdate);
	}

	public void getTripDeals() {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<Provider> providers = tourGuideService.getTripDeals(user);

		tourGuideService.tracker.stopTracking();

		assertEquals(10, providers.size());
	}
	
	
}
